import 'package:flutter/material.dart';
import 'package:hello_dunia/detailData.dart';

import 'databaseHelper/dbHelper.dart';
import 'databaseHelper/photoModel.dart';

class ListData extends StatefulWidget {
  @override
  _ListDataState createState() => _ListDataState();
}

class _ListDataState extends State<ListData> {
  DBHelper dbHelper;
  List<Photo> images;

  @override
  void initState() {
    super.initState();
    images = [];
    dbHelper = DBHelper();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('List Data'),
        centerTitle: true,
      ),
      body: FutureBuilder(
        future: dbHelper.getAllClients(),
        builder: (BuildContext context, snapshot) {
          if (snapshot.hasData) {
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (BuildContext context, int index) {
                Photo item = snapshot.data[index];
                return InkWell(
                  child: ListTile(
                    title: Text(item.tanggal.toString()),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => DetailData(
                              item.photoname, item.tanggal, item.latlong)),
                    );
                  },
                );
              },
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
