import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'dart:convert';

class Utility {
  static Image imageFromBase64String(String base64String) {
    return Image.memory(
      base64Decode(base64String),
      fit: BoxFit.fill,
    );
  }

  static Uint8List dataFromBase64String(String base64String) {
    return base64Decode(base64String);
  }

  static String base64String(Uint8List data) {
    return base64Encode(data);
  }

  static txtnya(String text) {
    return txtnya(text);
  }
}

class Photo {
  int id;
  String photoname;
  String latlong;
  String tanggal;

  Photo(this.id, this.photoname, this.latlong, this.tanggal);

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'id': id,
      'photo_name': photoname,
      'latlong': latlong,
      'tanggal': tanggal,
    };
    return map;
  }

  Photo.fromMap(Map<String, dynamic> map) {
    id = map['id'];
    photoname = map['photo_name'];
    latlong = map['latlong'];
    tanggal = map['tanggal'];
  }
}
