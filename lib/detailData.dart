import 'package:flutter/material.dart';

import 'databaseHelper/photoModel.dart';

class DetailData extends StatefulWidget {
  final String data;
  final String tgl;
  final String lat;
  DetailData(this.data, this.tgl, this.lat);
  @override
  _DetailDataState createState() => _DetailDataState();
}

class _DetailDataState extends State<DetailData> {
  @override
  void initState() {
    super.initState();
    print(widget.data);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Detail"),
      ),
      body: Column(
        children: [
          Container(
              padding: EdgeInsets.all(16),
              child: Utility.imageFromBase64String(widget.data)),
          Text(widget.tgl),
          Text(widget.lat),
        ],
      ),
    );
  }
}
