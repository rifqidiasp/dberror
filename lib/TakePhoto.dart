import 'package:flutter/material.dart';
import 'package:hello_dunia/databaseHelper/dbHelper.dart';
import 'package:hello_dunia/databaseHelper/photoModel.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'dart:async';
import 'package:geolocator/geolocator.dart';

class TakePhoto extends StatefulWidget {
  @override
  _TakePhotoState createState() => _TakePhotoState();
}

class _TakePhotoState extends State<TakePhoto> {
  DateTime dates = DateTime.now();
  Future<File> imageFile;
  Image image;
  File gambar;
  DBHelper dbHelper;
  List<Photo> images;

  String imgString = '';
  String latlongs = '';
  String tanggal = '';

  var geolocator = Geolocator();
  var locationOptions =
      LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);

  Future<dynamic> latlong() async {
    tanggal = dates.toIso8601String();
    await Geolocator()
        .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high)
        .then((value) {
      latlongs = value.latitude.toString() + "," + value.longitude.toString();
      pickImageFromGallery(ImageSource.camera);
    });

    return "sukses";
  }

  @override
  void initState() {
    super.initState();
    images = [];
    dbHelper = DBHelper();
  }

  pickImageFromGallery(ImageSource source) {
    setState(() {
      imageFile = ImagePicker.pickImage(source: source);
      imageFile.then((imgFile) {
        imgString = Utility.base64String(imgFile.readAsBytesSync());
      });
    });
  }

  saveData() {
    Photo photo = Photo(0, imgString, latlongs, tanggal);
    dbHelper.save(photo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Take photo'),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Flexible(child: showImage()),
            SizedBox(
              height: 20,
            ),
            FlatButton(
              onPressed: () {
                latlong();
              },
              child: Text('Take Photo', style: TextStyle(color: Colors.blue)),
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.blue, width: 1, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(50)),
            ),
            SizedBox(
              height: 10,
            ),
            FlatButton(
              onPressed: () {
                print(imgString);
                if (imgString.trim() != '' || imgString != null) {
                  saveData();
                }
              },
              child: Text('Save Photo', style: TextStyle(color: Colors.blue)),
              textColor: Colors.white,
              shape: RoundedRectangleBorder(
                  side: BorderSide(
                      color: Colors.blue, width: 1, style: BorderStyle.solid),
                  borderRadius: BorderRadius.circular(50)),
            ),
          ],
        ),
      ),
    );
  }

  Widget showImage() {
    return FutureBuilder<File>(
      future: imageFile,
      builder: (BuildContext context, AsyncSnapshot<File> snapshot) {
        if (snapshot.connectionState == ConnectionState.done &&
            snapshot.data != null) {
          return Image.file(
            snapshot.data,
            width: 300,
            height: 300,
          );
        } else if (snapshot.error != null) {
          return const Text(
            'Error Picking Image',
            textAlign: TextAlign.center,
          );
        } else {
          return Image.asset(
            'images/photo.png',
            width: 300,
            height: 300,
          );
        }
      },
    );
  }
}
